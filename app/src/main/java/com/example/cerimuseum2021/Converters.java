package com.example.cerimuseum2021;

import androidx.room.TypeConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Converters
{
    //On met ici des fonctions qui serviront a remplir la BDD inserer et redonner
    @TypeConverter
    /*
    creation d'une string vide qui par la suite va recuperer les informations retenues par categories dans le foreach
    que l'on va concaténer pour ensuite retourner le string desormais rempli.
     */

    public String fromList(List<String> categories)
    {
        String string = "";
        for (String recuperation: categories)
        {
            string += recuperation + ",";
        }
        return string;
    }

    /*
    Creation de la fonction inverse qui renvoie la liste de string concaténée pour chaque List<String>
    On part d'un string pour en faire une liste.
     */
    @TypeConverter
    public List<String> toList(String retourInformations)
    {
        List<String> liste = new ArrayList<>(); // ArrayList instancie le tableau. la liste ici hérite de ArrayList
        List<String> listeString = Arrays.asList(retourInformations.split(",")); // on créé une liste qui va contenir retourInformations découpé
        for(String string : listeString)
        {
            liste.add(string);
        }
        return liste;
    }

    /*
        Ici on créer une string vide dans laquelle on va convertir et recuperer une liste d'integer.
     */
    @TypeConverter
    public String fromListInteger(List<Integer> listInteger)
    {

        String string = "";
        for(Integer fonctionnement : listInteger)
        {
            string += fonctionnement.toString()+","; // fonctionnement est un int que l'on convertie ici en chaine de caractere que l'on separe ensuite par une virgule cf plus haut.
        }
        return string.substring(0, string.length()-1); // -1 afin de ne pas avoir le caractère en trop à la fin.
    }

    @TypeConverter
    public List<Integer> toListInteger(String retourFonctionnement)
    {
        List<Integer> listInteger = new ArrayList<>();;
        List<String> stringList = Arrays.asList(retourFonctionnement.split(","));

        for(String string : stringList)
        {
            listInteger.add(Integer.parseInt(string));
        }
        return listInteger;
    }

    /*
        recupere les infos de la map, si elle n'est pas nulle on fait un foreach dans lequel on recupere les informations de la map
        entrySet recupere toutes les clés dont on sait qu'elles contiennent une valeur (de ma map)
        ca permet de pouvoir entrer et recup les infos de la map
        https://www.geeksforgeeks.org/hashmap-entryset-method-in-java/
     */
    @TypeConverter
    public String fromMap(Map<String, String> mapString)
    {
        if(mapString!=null)
        {
            String string = "";
            for(Map.Entry<String, String> recuperationMap : mapString.entrySet())
            {
                string += recuperationMap.getKey() + "," + recuperationMap.getValue();
            }
            return string.substring(0, string.length()-1); // -1 pour le caractere en trop a la fin
        }
        else
        {
            return null;
        }
    }

    /*
    Cette fonction passe d'un string à une map de la récupération faite avant.
     */
    @TypeConverter
    public Map<String, String> toMap(String images)
    {
        Map<String, String> map=new HashMap<>();
        if(images!=null)
        {
            List<String> mapStringList = Arrays.asList(images.split(","));
            map.put(mapStringList.get(0), mapStringList.get(1));
        }
        return map;
    }
}
