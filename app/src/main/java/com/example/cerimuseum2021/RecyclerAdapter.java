package com.example.cerimuseum2021;

import android.content.ClipData;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private static final String TAG = com.example.cerimuseum2021.RecyclerAdapter.class.getSimpleName();
    private static int TYPE_RIGHT = 1;
    private static int TYPE_LEFT = 2;

    private List<Item> itemList;
    private ListViewModel listViewModel;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position)
    {
        View view = new View(viewGroup.getContext());
        if(position==TYPE_RIGHT)
        {
            view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout, viewGroup, false);
            return new RightViewHolder(view);
        }
        if(position==TYPE_LEFT)
        {
            view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout_alt, viewGroup, false);
            return new LeftViewHolder(view);
        }
        return new ViewHolder(view);
    }

    class RightViewHolder extends RecyclerView.ViewHolder
    {
        TextView itemTitle;
        TextView itemDetail;
        TextView itemMarque;
        ImageView itemIcone;

        ActionMode actionMode;
        long idSelectedLongClick;

        RightViewHolder(View itemView)
        {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.nom);
            itemDetail = itemView.findViewById(R.id.categories);
            itemIcone = itemView.findViewById(R.id.image);
            itemMarque = itemView.findViewById(R.id.marque);
        };

    }

    class LeftViewHolder extends RecyclerView.ViewHolder
    {
        TextView itemTitle;
        TextView itemDetail;
        TextView itemMarque;
        ImageView itemIcon;

        ActionMode actionMode;
        long idSelectedLongClick;

        LeftViewHolder(View itemView)
        {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.nom);
            itemDetail = itemView.findViewById(R.id.categories);
            itemIcon = itemView.findViewById(R.id.image);
            itemMarque = itemView.findViewById(R.id.marque);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if(getItemViewType(i)==TYPE_RIGHT){
            ((RightViewHolder) viewHolder).itemTitle.setText(itemList.get(i).getNom());
            ((RightViewHolder) viewHolder).itemDetail.setText(itemList.get(i).getCategories().toString());
            if(itemList.get(i).getMarque()!=null) {
                ((RightViewHolder) viewHolder).itemMarque.setText(itemList.get(i).getMarque());
            }
            Glide.with(viewHolder.itemView)
                    .load(itemList.get(i).getThumbnailURL())
                    .into(((RightViewHolder) viewHolder).itemIcone);
        }
        if(getItemViewType(i)==TYPE_LEFT){
            ((LeftViewHolder) viewHolder).itemTitle.setText(itemList.get(i).getNom());
            ((LeftViewHolder) viewHolder).itemDetail.setText(itemList.get(i).getCategories().toString());
            if(itemList.get(i).getMarque()!=null) {
                ((LeftViewHolder) viewHolder).itemMarque.setText(itemList.get(i).getMarque());
            }
            Glide.with(viewHolder.itemView)
                    .load(itemList.get(i).getThumbnailURL())
                    .into(((LeftViewHolder) viewHolder).itemIcon);
        }
    }

    @Override
    public int getItemCount()
    {
        return itemList == null ? 0 : itemList.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        if(position%2==1){
            return TYPE_RIGHT;
        }
        if(position%2==0){
            return TYPE_LEFT;
        }
        return 0;
    }

    public void setItemList(List<Item> items)
    {
        itemList = items;
        notifyDataSetChanged();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;
        ImageView itemIcon;


        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.nom);
            itemDetail = itemView.findViewById(R.id.categories);
            itemIcon = itemView.findViewById(R.id.image);
        }
    }
}