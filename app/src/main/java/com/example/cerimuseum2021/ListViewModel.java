package com.example.cerimuseum2021;

import android.app.Application;
import android.content.ClipData;

import androidx.lifecycle.AndroidViewModel;

import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class ListViewModel extends AndroidViewModel {
    private Application repository;
    private LiveData<List<Item>> allItems;
    private MutableLiveData<List<Item>> mutableAllItems;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;
    private MutableLiveData<List<Section>> mutableItemsBycategories;

    public ListViewModel(Application application) throws ExecutionException, InterruptedException {
        super(application);
        repository = ItemRepository.get(application);
    }

    LiveData<List<Item>> getAllItems() {
        return allItems;
    }

    MutableLiveData<List<Item>> getAllItemsMutable() {
        return mutableAllItems;
    }

}