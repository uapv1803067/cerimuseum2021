package com.example.cerimuseum2021;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class ItemRepository
{

    private final MuseumWSInterface apinterface;

    public ItemRepository(Application repositoryItemApp)
    {
        ItemRoomDatabase dataBase = ItemRoomDatabase.getDatabase(repositoryItemApp);
        ItemDao itemAccessObject = dataBase.itemAccessObject();
        MutableLiveData<Object> selectedItem = new MutableLiveData<>();
        MutableLiveData<Object> mutableAllItems = new MutableLiveData<>();
        MutableLiveData<Object> isLoading = new MutableLiveData<>();
        MutableLiveData<Object> webServiceThrowable = new MutableLiveData<>();
        Retrofit retrofit=new Retrofit.Builder().baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/").addConverterFactory(MoshiConverterFactory.create()).build();

        apinterface = retrofit.create(MuseumWSInterface.class);
        loadCollectionVersion();
        AsyncTask.execute(() -> itemAccessObject.deleteAll());
        loadCollection();
        ItemRepository itemDao = null;
        //LiveData<List<Item>> allItems = itemDao.getAllItems();

    }
/*
    public static Application get(Application repositoryItemApp)
    {
    }
*/
    private static void loadCollection() {
    }

    private static void loadCollectionVersion() {
    }

    /*public LiveData<List<Item>> getAllItems()
    {
        return allItems;
    }

    public MutableLiveData<List<Item>> getAllItemsMutable()
    {
        return mutableAllItems;
    }
*/

    public void getAllItemsByName()
    {
        getAllItemsByName();
    }

    public void getAllItemsByYear()
    {
        getAllItemsByYear();
    }

    public void searchItems(String search)
    {

    }
/*
    public MutableLiveData<Throwable> getWebServiceThrowable()
    {
    }

    public MutableLiveData<Boolean> getIsLoading() {
    }
 */
}
