package com.example.cerimuseum2021;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface MuseumWSInterface
{
    @Headers("Accept: application/geo+json")
    @GET("collection")
    Call<Map<String, ItemResponse>> getCollection();

    @GET("versionBase")
    Call<Float> getCollectionVersion();

}
