package com.example.cerimuseum2021;
// REVOIR CETTE PAGE CREE A PART DITEMROOMDATABASE
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
@Dao
public interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Item item);

    @Query("DELETE FROM items_table")
    void deleteAll();

    @Query("SELECT * from items_table")
    LiveData<List<Item>> getAllItems();

    @Query("SELECT * from items_table ORDER BY year DESC")
    List<Item> getAllItemsByYear();

    @Query("SELECT * from items_table ORDER BY name ASC")
    List<Item> getAllItemsByName();

    @Query("SELECT * from items_table WHERE id_item LIKE :search or name LIKE :search or categories LIKE :search or description LIKE :search or brand LIKE :search or technicalDetails LIKE :search")
    List<Item> searchItems(String search);

    @Query("DELETE FROM items_table WHERE _id = :id")
    void deleteItem(double id);

    @Query("SELECT * FROM items_table WHERE _id = :id")
    Item getItemById(double id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item item);

}
