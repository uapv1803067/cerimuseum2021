package com.example.cerimuseum2021;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Database(entities = {Item.class}, version = 2, exportSchema = false)
public abstract class ItemRoomDatabase extends RoomDatabase {

    private static final String TAG = ItemRoomDatabase.class.getSimpleName();

    public abstract ItemDao itemAccessObject();

    private static ItemRoomDatabase INSTANCIATION;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static ItemRoomDatabase getDatabase(final Context context)
    {
        if (INSTANCIATION == null)
        {
            synchronized (ItemRoomDatabase.class)
            {
                if (INSTANCIATION == null)
                {
                    INSTANCIATION = Room.databaseBuilder(context.getApplicationContext(),
                            ItemRoomDatabase.class,"items_database").fallbackToDestructiveMigration().build();
                }
            }
        }
        return INSTANCIATION;
    }
}

