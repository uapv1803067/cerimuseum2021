package com.example.cerimuseum2021;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;
import java.util.Map;

public class Item
{
    // Chaque objet à récupérer de demolia
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="_id")
    private long id;

    @NonNull
    @ColumnInfo(name="id_item")
    private String id_item;

    @NonNull
    @ColumnInfo(name="nom")
    private String nom;

    @NonNull
    @ColumnInfo(name="categories")
    @TypeConverters(Converters.class)
    private List<String> categories;

    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name="timeFrame")
    @TypeConverters(Converters.class)
    private List<Integer> timeFrame;

    @ColumnInfo(name="annee")
    private Integer annee;

    @ColumnInfo(name="marque")
    private String marque;

    @ColumnInfo(name="technicalDetails")
    @TypeConverters(Converters.class)
    private List<String> technicalDetails;

    @TypeConverters(Converters.class)
    @ColumnInfo(name="images")
    private Map<String, String> images;

    @ColumnInfo(name="fonctionnement")
    private Integer fonctionnement;

    // alt + inser pour getter et setter
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @NonNull
    public String getId_item() {
        return id_item;
    }

    public void setId_item(@NonNull String id_item) {
        this.id_item = id_item;
    }

    @NonNull
    public String getNom() {
        return nom;
    }

    public void setNom(@NonNull String nom) {
        this.nom = nom;
    }

    @NonNull
    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(@NonNull List<String> categories) {
        this.categories = categories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Integer> getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(List<Integer> timeFrame) {
        this.timeFrame = timeFrame;
    }

    public Integer getAnnee() {
        return annee;
    }

    public void setAnnee(Integer annee) {
        this.annee = annee;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public List<String> getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(List<String> technicalDetails) {
        this.technicalDetails = technicalDetails;
    }
    public String getThumbnailURL(){
        return "https://demo-lia.univ-avignon.fr/cerimuseum/items/"+id_item+"/thumbnail";
    }

    public Map<String, String> getImages() {
        return images;
    }

    public void setImages(Map<String, String> images) {
        this.images = images;
    }

    public Integer getFonctionnement() {
        return fonctionnement;
    }

    public void setFonctionnement(Integer fonctionnement) {
        this.fonctionnement = fonctionnement;
    }

    //alt + inser pour le constructeur
    public Item(long id, @NonNull String id_item, @NonNull String nom, @NonNull List<String> categories, String description, List<Integer> timeFrame, Integer annee, String marque, List<String> technicalDetails, Map<String, String> images, Integer fonctionnement) {
        this.id = id;
        this.id_item = id_item;
        this.nom = nom;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.annee = annee;
        this.marque = marque;
        this.technicalDetails = technicalDetails;
        this.images = images;
        this.fonctionnement = fonctionnement;
    }
    // idem pour la methode to String comme ça tout sera bien lu
    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", id_item='" + id_item + '\'' +
                ", nom='" + nom + '\'' +
                ", categories=" + categories +
                ", description='" + description + '\'' +
                ", timeFrame=" + timeFrame +
                ", annee=" + annee +
                ", marque='" + marque + '\'' +
                ", technicalDetails=" + technicalDetails +
                ", images=" + images +
                ", fonctionnement=" + fonctionnement +
                '}';
    }
}
